import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.neighbors import LocalOutlierFactor
from sklearn.metrics import precision_recall_fscore_support
import pickle

class LocalOutlierFactorImpl:

    def run(self, ground_truth, y_test, PercUnregister, X):
        # minRE = 10
        # maxRE = 1100
        # EpsF1 = []
        #
        # for TryRE in range(minRE, maxRE, 10):
        #     clf = LocalOutlierFactor(n_neighbors=TryRE, contamination=PercUnregister, algorithm='auto',
        #                              leaf_size=30, metric='minkowski', p=2, metric_params=None)
        #     x = X
        #     y_pred = clf.fit_predict(x)
        #     n_errors = (y_pred != ground_truth).sum()
        #     X_scores = clf.negative_outlier_factor_
        #     y_predLOF = y_pred.copy()
        #     y_predDF = pd.DataFrame(y_predLOF)
        #     y_predDF[y_predDF[0] == 1] = 0
        #     y_predDF[y_predDF[0] == -1] = 1
        #     y_predLOF = y_predDF.values
        #     y_predLOF = np.ravel(y_predLOF)
        #     precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF,
        #                                                                               average='binary')
        #     print("F1 score on test", round(fbeta_score, 4), " with num neighbors ", TryRE)
        #     EpsF1.append([TryRE, round(fbeta_score, 4)])
        # EpsF1df = pd.DataFrame(EpsF1, columns=['Num_Neighbors', 'F1'])
        # EpsF1df.head()
        # EpsF1df.plot.line("Num_Neighbors", "F1")
        # plt.xlim(1, 1100)
        # plt.title("F1 vs Num_Neighbors")
        # plt.show()
        #
        # column = EpsF1df["F1"]
        # max_value = column.max()
        # index_max_value = column.idxmax()
        # n_neigh = EpsF1df.iloc[index_max_value, 0]

        # AFTER
        clf = LocalOutlierFactor(n_neighbors=1, contamination=PercUnregister, algorithm='auto',
                                 leaf_size=30, metric='minkowski', p=2, metric_params=None, novelty=True)
        clf.fit(X)
        y_pred = clf.predict(X)
        n_errors = (y_pred != ground_truth).sum()
        X_scores = clf.negative_outlier_factor_
        # print('accuracy ' , round(1 - (n_errors / X.shape[0]),4))
        print(n_errors)
        y_predLOF = y_pred.copy()
        y_predDF = pd.DataFrame(y_predLOF)
        y_predDF[y_predDF[0] == 1] = 0
        y_predDF[y_predDF[0] == -1] = 1
        y_predLOF = y_predDF.values
        y_predLOF = np.ravel(y_predLOF)
        # F1 Score
        precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF, average='binary')
        print("precision ", round((precision), 4))
        print("recall ", round((recall), 4))
        print("F1 score on Test", round((fbeta_score), 4))
        filename = '../postgresql/service/lof_finalized_model.sav'
        pickle.dump(clf, open(filename, 'wb'))
        return y_predLOF