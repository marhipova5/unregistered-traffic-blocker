import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.svm import OneClassSVM
from sklearn.metrics import precision_recall_fscore_support
import pickle

class OneClassSVMImpl:

    def run(self, ground_truth, y_test, PercUnregister, X):
        minRE = 0.01
        maxRE = 0.99
        EpsF1 = []
        for TryRE in np.arange(minRE, maxRE, 0.05):
            OneSVM = OneClassSVM(kernel='rbf', nu=TryRE, degree=3, gamma=0.1, max_iter=-1)
            # Делаем предсказания на тестовом наборе
            y_pred = OneSVM.fit_predict(X)
            n_errors = (y_pred != ground_truth).sum()
            y_predLOF = y_pred.copy()
            y_predDF = pd.DataFrame(y_predLOF)
            y_predDF[y_predDF[0] == 1] = 0
            y_predDF[y_predDF[0] == -1] = 1
            y_predLOF = y_predDF.values
            y_predLOF = np.ravel(y_predLOF)

            precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF,
                                                                                      average='binary')
            print("F1 score on test", round(fbeta_score, 4), " with nu ", TryRE)
            EpsF1.append([TryRE, round(fbeta_score, 4)])
        EpsF1df = pd.DataFrame(EpsF1, columns=['nu', 'F1'])
        EpsF1df.head()
        EpsF1df.plot.line("nu", "F1")
        plt.xlim(minRE, maxRE)
        plt.title("F1 vs nu")
        plt.show()
        column = EpsF1df["F1"]
        max_value = column.max()
        index_max_value = column.idxmax()
        nu = EpsF1df.iloc[index_max_value, 0]
        print("nu: " + str(nu))

        OneSVM = OneClassSVM(kernel='rbf', nu=nu, degree=3, gamma=0.1, max_iter=-1)
        y_pred = OneSVM.fit_predict(X)
        n_errors = (y_pred != ground_truth).sum()
        print(n_errors)
        y_predLOF = y_pred.copy()
        y_predDF = pd.DataFrame(y_predLOF)
        y_predDF[y_predDF[0] == 1] = 0
        y_predDF[y_predDF[0] == -1] = 1
        y_predLOF = y_predDF.values
        y_predLOF = np.ravel(y_predLOF)

        # F1 Score
        # print("F1 score", round(f1_score(y_valid,pred, average='binary'), 4))
        precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF, average='binary')
        print("precision ", round((precision), 4))
        print("recall ", round((recall), 4))
        print("F1 score on Test", round((fbeta_score), 4))
        # save the dto to disk
        filename = '../file/model/one_class_svm_finalized_model.sav'
        pickle.dump(OneSVM, open(filename, 'wb'))
        return y_predLOF


# cases = len(df)
    # nonfraud_count = len(df[df.Class == 0])
    # fraud_count = len(df[df.Class == 1])
    # fraud_percentage = round(fraud_count / nonfraud_count * 100, 2)
    #
    # print(cl('CASE COUNT', attrs=['bold']))
    # print(cl('--------------------------------------------', attrs=['bold']))
    # print(cl('Total number of cases are {}'.format(cases), attrs=['bold']))
    # print(cl('Number of Non-fraud cases are {}'.format(nonfraud_count), attrs=['bold']))
    # print(cl('Number of fraud cases are {}'.format(fraud_count), attrs=['bold']))
    # print(cl('Percentage of fraud cases is {}'.format(fraud_percentage), attrs=['bold']))
    # print(cl('--------------------------------------------', attrs=['bold']))
    #
    # nonfraud_cases = df[df.Class == 0]
    # fraud_cases = df[df.Class == 1]
    #
    # print(cl('CASE AMOUNT STATISTICS', attrs=['bold']))
    # print(cl('--------------------------------------------', attrs=['bold']))
    # print(cl('NON-FRAUD CASE AMOUNT STATS', attrs=['bold']))
    # print(nonfraud_cases.Amount.describe())
    # print(cl('--------------------------------------------', attrs=['bold']))
    # print(cl('FRAUD CASE AMOUNT STATS', attrs=['bold']))
    # print(fraud_cases.Amount.describe())
    # print(cl('--------------------------------------------', attrs=['bold']))

    # matrix = df.to_numpy()
    # Defining the dto and prediction
    # nu = 0,03 означает, что алгоритм определит 3% данных как выбросы.
    # svm = OneClassSVM(kernel='rbf', gamma=0.001, nu=0.03)
    # print(svm)
