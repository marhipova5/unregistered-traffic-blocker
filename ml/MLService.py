import pandas as pd
import numpy as np
import itertools
import matplotlib.pyplot as plt
from postgresql.service.LldService import LldService
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix
from ml.IsolationForestImpl import IsolationForestImpl
from ml.EllipticEnvelopeImpl import EllipticEnvelopeImpl
from ml.OneClassSVMImpl import OneClassSVMImpl
from ml.LocalOutlierFactorImpl import LocalOutlierFactorImpl
from sklearn.neighbors import LocalOutlierFactor
import pickle

lldService = LldService()
isolationForest = IsolationForestImpl()
ellipticEnvelope = EllipticEnvelopeImpl()
oneClassSvm = OneClassSVMImpl()
localOutlierFactor = LocalOutlierFactorImpl()

def build_confusion_matrix(Y_test, y_predLOF):
    CM = confusion_matrix(Y_test, y_predLOF)
    tn, fp, fn, tp = confusion_matrix(Y_test, y_predLOF).ravel()
    print(CM)
    print("_" * 50)
    print("TP ", tp)
    print("FP ", fp)
    print("TN ", tn)
    print("FN ", fn)
    plot_confusion_matrix(CM, normalize=False, target_names=['Register', 'Unregister'],
                          title="Confusion Matrix")

def plot_confusion_matrix(cm, target_names, title='Confusion matrix', cmap=None,
                          normalize=False):
    accuracy = np.trace(cm) / float(np.sum(cm))
    misclass = 1 - accuracy

    if cmap is None:
        cmap = plt.get_cmap('Blues')

    plt.figure(figsize=(8, 6))
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()

    if target_names is not None:
        tick_marks = np.arange(len(target_names))
        plt.xticks(tick_marks, target_names, rotation=45)
        plt.yticks(tick_marks, target_names)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    thresh = cm.max() / 1.5 if normalize else cm.max() / 2
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        if normalize:
            plt.text(j, i, "{:0.4f}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")
        else:
            plt.text(j, i, "{:,}".format(cm[i, j]),
                     horizontalalignment="center",
                     color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label\naccuracy={:0.4f}; misclass={:0.4f}'.format(accuracy, misclass))
    plt.show()


if __name__ == "__main__":
    register = lldService.get_matrix('register_lld', 'diplom2', 'postgres', '1234', 'localhost', '5432')
    register = register.fillna(0)
    unregister = lldService.get_matrix('unregister_lld', 'diplom2', 'postgres', '1234', 'localhost', '5432')
    unregister = unregister.fillna(0)
    result = pd.concat([register, unregister])
    print("Информация о данных:")
    print("data ", result.shape)
    print("Register_data ", register.shape)
    print("Unregister_data ", unregister.shape)
    print("Percent unregister ", round(100 * len(unregister) / len(result), 3), "%")
    print(result.head())
    print("_" * 50)

    X_train, X_inliers = train_test_split(register, test_size=0.30, random_state=1)
    x_test = np.concatenate([X_inliers[:], unregister[:]])
    X_outliers = unregister[:]
    y_test = np.concatenate([np.zeros(len(X_inliers)), np.ones(len(unregister))])
    X = np.r_[X_inliers, X_outliers]
    n_outliers = len(X_outliers)
    ground_truth = np.ones(len(X), dtype=int)
    ground_truth[-n_outliers:] = -1
    PercUnregister = n_outliers / x_test.shape[0]  # Доля выбросов
    print(PercUnregister)
    print('X_inliers ', X_inliers.shape)
    print('X_outliers ', X_outliers.shape)
    print('X ', X.shape)
    print('n_outliers ', n_outliers)
    print('percent unregister query in test: ', PercUnregister)

    clf = LocalOutlierFactor(n_neighbors=1, contamination=PercUnregister, algorithm='auto',
                             leaf_size=30, metric='minkowski', p=2, metric_params=None, novelty=True)
    clf.fit(X_train)
    filename = '../postgresql/service/lof_finalized_model.sav'
    pickle.dump(clf, open(filename, 'wb'))
    # y_pred = clf.predict(X)


    # precision, recall, fbeta_score, support = precision_recall_fscore_support(x_test, y_test, average='binary')
    # print("precision ", round((precision), 4))
    # print("recall ", round((recall), 4))
    # print("F1 score on Test", round((fbeta_score), 4))


    # # --------------------- One Class SVM -------------------
    # y_pred_os = oneClassSvm.run(ground_truth, y_test, PercUnregister, X)
    # precision_os, recall_os, fbeta_score_os, support = precision_recall_fscore_support(y_test, y_pred_os, average='binary')
    # print("precision ", round((precision_os), 4))
    # print("recall ", round((recall_os), 4))
    # print("F1 score on Test", round((fbeta_score_os), 4))
    # build_confusion_matrix(y_test, y_pred_os)
    #
    # # ---------------------- Isolation Forest ---------------
    # y_pred_if = isolationForest.run(ground_truth, y_test, PercUnregister, X)
    # precision_if, recall_if, fbeta_score_if, support = precision_recall_fscore_support(y_test, y_pred_if, average='binary')
    # print("precision ", round((precision_if), 4))
    # print("recall ", round((recall_if), 4))
    # print("F1 score on Test", round((fbeta_score_if), 4))
    # build_confusion_matrix(y_test, y_pred_if)
    #
    #
    # # ----------------------------- Elliptic Envelope --------
    # y_pred_ee = ellipticEnvelope.run(ground_truth, y_test, PercUnregister, X)
    # precision_ee, recall_ee, fbeta_score_ee, support = precision_recall_fscore_support(y_test, y_pred_ee, average='binary')
    # print("precision ", round((precision_ee), 4))
    # print("recall ", round((recall_ee), 4))
    # print("F1 score on Test", round((fbeta_score_ee), 4))
    # build_confusion_matrix(y_test, y_pred_ee)

    # --------------------- LOF ---------------------------
    # y_pred_lof = localOutlierFactor.run(ground_truth, y_test, PercUnregister, X)
    # precision_lof, recall_lof, fbeta_score_lof, support = precision_recall_fscore_support(y_test, y_pred_lof, average='binary')
    # print("precision ", round((precision_lof), 4))
    # print("recall ", round((recall_lof), 4))
    # print("F1 score on Test", round((fbeta_score_lof), 4))
    # build_confusion_matrix(y_test, y_pred_lof)

    # f1_all = [fbeta_score_os, fbeta_score_if, fbeta_score_ee, fbeta_score_lof]
    # f1_max = max(f1_all)
    # print("f1_max: " + str(f1_max))




