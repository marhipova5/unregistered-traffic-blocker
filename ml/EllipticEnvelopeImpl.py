import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.covariance import EllipticEnvelope
from sklearn.metrics import precision_recall_fscore_support
import pickle

class EllipticEnvelopeImpl:

    def run(self, ground_truth, y_test, PercUnregister, X):
        # Optimize support_fraction hyper paramter for best F1
        minRE = 0.4
        maxRE = 0.99
        EpsF1 = []
        for TryRE in np.arange(minRE, maxRE, 0.01):
            cov = EllipticEnvelope(support_fraction=TryRE, contamination=PercUnregister,
                                   store_precision=True, assume_centered=False)
            x = X
            y_pred = cov.fit_predict(x)
            n_errors = (y_pred != ground_truth).sum()

            y_predLOF = y_pred.copy()
            y_predDF = pd.DataFrame(y_predLOF)
            y_predDF[y_predDF[0] == 1] = 0
            y_predDF[y_predDF[0] == -1] = 1
            y_predLOF = y_predDF.values
            y_predLOF = np.ravel(y_predLOF)
            precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF,
                                                                                      average='binary')
            print("F1 score on test", round(fbeta_score, 4), " with support_fraction ", TryRE)
            EpsF1.append([TryRE, round(fbeta_score, 4)])

        EpsF1df = pd.DataFrame(EpsF1, columns=['Support_Fraction', 'F1'])
        EpsF1df.head()
        EpsF1df.plot.line("Support_Fraction", "F1")
        plt.xlim(minRE, maxRE)
        plt.title("F1 vs Support_Fraction")
        plt.show()
        column = EpsF1df["F1"]
        max_value = column.max()
        index_max_value = column.idxmax()
        sup_frac = EpsF1df.iloc[index_max_value, 0]
        print(sup_frac)

        # ------------ После улучшения значения support_fraction ------------
        cov = EllipticEnvelope(support_fraction=sup_frac, contamination=PercUnregister,
                               store_precision=True, assume_centered=False)
        y_pred = cov.fit_predict(X)
        n_errors = (y_pred != ground_truth).sum()
        print(n_errors)
        y_predLOF = y_pred.copy()
        y_predDF = pd.DataFrame(y_predLOF)
        y_predDF[y_predDF[0] == 1] = 0
        y_predDF[y_predDF[0] == -1] = 1
        y_predLOF = y_predDF.values
        y_predLOF = np.ravel(y_predLOF)
        precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF, average='binary')
        print("precision ", round((precision), 4))
        print("recall ", round((recall), 4))
        print("F1 score on Test", round((fbeta_score), 4))
        filename = '../file/model/ee_finalized_model.sav'
        pickle.dump(cov, open(filename, 'wb'))
        return y_predLOF