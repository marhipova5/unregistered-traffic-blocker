import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import IsolationForest
from sklearn.metrics import precision_recall_fscore_support
import pickle

class IsolationForestImpl:
    # if __name__ == "__main__":
    def get_max_num_est(self, ground_truth, y_test, PercUnregister, X):
        # Optimize Num Estimators hyper paramter for best F1
        minRE = 50
        maxRE = 1150
        EpsF1 = []
        for TryRE in np.arange(minRE, maxRE, 50):
            isf = IsolationForest(n_estimators=TryRE, max_features=1.0, max_samples=1.0,
                                  bootstrap=False, random_state=22, contamination=PercUnregister,
                                  verbose=0)
            x = X
            y_pred = isf.fit_predict(x)
            n_errors = (y_pred != ground_truth).sum()
            y_predLOF = y_pred.copy()
            y_predDF = pd.DataFrame(y_predLOF)
            y_predDF[y_predDF[0] == 1] = 0
            y_predDF[y_predDF[0] == -1] = 1
            y_predLOF = y_predDF.values
            y_predLOF = np.ravel(y_predLOF)
            precision, recall, fbeta_score, support = precision_recall_fscore_support(y_test, y_predLOF,
                                                                                      average='binary')
            print("F1 score on test", round(fbeta_score, 4), " with num_estimators ", TryRE)
            EpsF1.append([TryRE, round(fbeta_score, 4)])

        EpsF1df = pd.DataFrame(EpsF1, columns=['Num_Estimators', 'F1'])
        EpsF1df.head()
        EpsF1df.plot.line("Num_Estimators", "F1")
        plt.xlim(minRE, maxRE)
        plt.title("F1 vs Num_Estimators")
        plt.show()
        column = EpsF1df["F1"]
        max_value = column.max()
        index_max_value = column.idxmax()
        num_est = EpsF1df.iloc[index_max_value, 0]
        print("num_est: " + str(num_est))
        return num_est

        # AFTER
    def run(self, ground_truth, y_test, PercUnregister, X):
        num_est = self.get_max_num_est(ground_truth, y_test, PercUnregister, X)
        isf = IsolationForest(n_estimators=num_est, max_features=1.0, max_samples=1.0,
                              bootstrap=False, random_state=22, contamination=PercUnregister,
                              verbose=0)
        y_pred = isf.fit_predict(X)
        n_errors = (y_pred != ground_truth).sum()
        print("n_errors: " + str(n_errors))
        y_predLOF = y_pred.copy()
        y_predDF = pd.DataFrame(y_predLOF)
        y_predDF[y_predDF[0] == 1] = 0
        y_predDF[y_predDF[0] == -1] = 1
        y_predLOF = y_predDF.values
        y_predLOF = np.ravel(y_predLOF)
        # F1 Score
        # print("F1 score", round(f1_score(y_valid,pred, average='binary'), 4))
        filename = '../file/model/isf_finalized_model.sav'
        pickle.dump(isf, open(filename, 'wb'))
        return y_predLOF
