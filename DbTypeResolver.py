import streamlit as st
import psycopg2
import mysql.connector
from postgresql.service.LldService import LldService as pg_lldService
from mysql2.service.LldService import LldService as my_lldService

pg_lldService = pg_lldService()
my_lldService = my_lldService()

class DbTypeResolver:

    def try_connect(self, db_type, host, port, db, user, password):
        if db_type == 'PostgreSQL':
            try:
                con = psycopg2.connect(dbname=db, user=user, password=password, host=host, port=port)
                st.success('Успешно подключены!')
            except Exception as err:
                st.error(
                    'Не удалось подключиться к базе данных. Проверьте, верно ли введены данные, и попробуйте еще раз.')
        if db_type == 'MySQL':
            try:
                cnx = mysql.connector.connect(user=user, dbname=db, host=host, port=port, password=password)
                st.success('Успешно подключены!')
            except mysql.connector.Error as err:
                st.error(
                    'Не удалось подключиться к базе данных. Проверьте, верно ли введены данные, и попробуйте еще раз.')
        return True


    def create_lld_table_and_get_sql_query_from_df(self, db_type, df, lld_tablename, counter_tablename, dbname, user, password, host, port):
        if db_type == 'PostgreSQL':
            df.columns = ['log_time', 'user_name', 'database_name', 'process_id', 'connection_from', 'session_id',
                          'session_line_num', 'command_tag', 'session_start_time', 'virtual_transaction_id',
                          'transaction_id', 'error_severity', 'sql_state_code', 'message', 'detail', 'hint',
                          'internal_query', 'internal_query_pos', 'context', 'query', 'query_pos', 'location',
                          'application_name']
            st.dataframe(df)
            pg_lldService.create_lld_table(lld_tablename, counter_tablename, dbname, user, password, host, port)
            pg_lldService.get_sql_query_from_df(df, lld_tablename, counter_tablename, dbname, user, password, host, port)
        elif db_type == 'MySQL':
            df.columns = []
            st.dataframe(df)


    def create_lld_and_safe_into_db(self, line, lld_tablename, counter_tablename, dbname, user, password, host, port):
        print()
