import base64
import pandas as pd
import streamlit as st
from DbTypeResolver import DbTypeResolver
from postgresql.service.LldService import LldService as pg_lldService
# from mysql2.service.LldService import LldService as my_lldService
from postgresql.service.GldService import GldService
from postgresql.service.MonitoringService import MonitoringService


pg_lldService = pg_lldService()
gldService = GldService()
resolver = DbTypeResolver()
monitoringService = MonitoringService()


def get_file_details(file_uploaded, isRegister):
    lld_tablename = ''
    counter_tablename = ''
    file_details = {"Имя файла": file_uploaded.name, "Тип файла": file_uploaded.type,
                    "Размер файла": file_uploaded.size}
    st.write(file_details)
    if isRegister is True:
        lld_tablename = 'register_lld'
        counter_tablename = 'register_counter'
    elif isRegister is False:
        lld_tablename = 'unregister_lld'
        counter_tablename = 'unregister_counter'
    if file_uploaded.type == "text/plain":
        raw_text = str(file_uploaded.read(), "utf-8")
        st.text(raw_text)
        for line in raw_text.splitlines():
            pg_lldService.create_lld_table(lld_tablename, counter_tablename, dbname, user, password, host, port)
            pg_lldService.create_lld_and_safe_into_db(line, lld_tablename, counter_tablename, dbname, user, password, host, port)
    elif file_uploaded.type == "text/csv" or file_uploaded.type == "application/vnd.ms-excel":
        df = pd.read_csv(file_uploaded)
        resolver.create_lld_table_and_get_sql_query_from_df(db_type, df, lld_tablename, counter_tablename,
                                                            dbname, user, password, host, port)



st.title('Блокировка незарегистрированного sql трафика на базы данных PostgreSQL и MySQL')
st.markdown('<h3>Выполните первоначальную настройку работы приложения</h3><br>', unsafe_allow_html=True)

connect_ok = False
file_ok = False


with st.form("choose_db_type"):
    db_type = st.radio('1. Выберите тип базы данных:',
                       ['PostgreSQL',
                        'MySQL'])
    submitted = st.form_submit_button("Выбрать")
    if submitted:
        st.success("Выбрано: " + db_type)

with st.form("db_data"):
    st.write('<text style="font-size:0.8rem">2. Введите данные для подключения к базе данных:</text>', unsafe_allow_html=True)
    host = st.text_input('Хост')
    port = st.text_input('Порт')
    dbname = st.text_input('База данных')
    user = st.text_input('Пользователь')
    password = st.text_input('Пароль', type="password")
    submitted = st.form_submit_button("Подключиться")
    if submitted:
        connect = resolver.try_connect(db_type, host, port, dbname, user, password)
        if db_type is None:
            st.error('Выберите тип базы данных!')

with st.form("db_gld"):
    st.write(
        '<text style="font-size:0.8rem">Если нужно загрузить GLD, то нажмите кнопку:</text>',
        unsafe_allow_html=True)
    submitted_gld = st.form_submit_button("Загрузить GLD")
    if submitted_gld:
        postgresql_gld = gldService.get_gld_values_from_xlsx_file()
        gldService.safe_gld_values_into_db(postgresql_gld, dbname, user, password, host, port)
        st.success("Готово!")


# if connect == True:
st.write('<text style="font-size:0.8rem">3. Для обучения модели на данных, пожалуйста, загрузите файлы с примером трафика вашего приложения:</text>', unsafe_allow_html=True)
file_uploaded = st.file_uploader("Зарегистрированный трафик", type=['txt', 'csv'])
if st.button("Подробнеe"):
    if file_uploaded is not None:
        get_file_details(file_uploaded, True)

file_uploaded2 = st.file_uploader("Незарегистрированный трафик", type=['txt', 'csv'])
if st.button("Подробнее"):
    if file_uploaded2 is not None:
        get_file_details(file_uploaded2, False)

st.write('<text style="font-size:0.8rem">Теперь приступим к обучению модели:</text>', unsafe_allow_html=True)
if st.button('Обучить модель'):
    st.write('<text style="font-size:0.8rem">Готово! Приятной работы!</text>', unsafe_allow_html=True)

# if file_ok == True:
with st.form("slider"):
    # st.write('<text style="font-size:0.8rem">4. Выберите минимальную вероятность принадлежности незарегистрированного запроса к зарегистрированному трафику</text>', unsafe_allow_html=True)
    st.write('<text style="font-size:0.8rem">4. Укажите комментарий, который используется для идентификации зарегистрированного трафика:</text>', unsafe_allow_html=True)
    pattern = st.text_input('Паттерн')
    submitted = st.form_submit_button("Сохранить")
    if submitted:
        finish = True
        st.success("Успешно!")

def get_table_download_link(df):
    csv = df.to_csv(index=False)
    b64 = base64.b64encode(csv.encode()).decode()  # some strings <-> bytes conversions necessary here
    href = f'<a href="data:file/csv;base64,{b64}" download="report.csv">Получить отчет</a>'
    return href

st.write('<text style="font-size:0.8rem">Приложение успешно настроено! Начать мониторинг базы данных?</text>', unsafe_allow_html=True)
if st.button('Начать мониторинг'):
    st.write('<text style="font-size:0.8rem">Готово! Приятной работы!</text>', unsafe_allow_html=True)
    monitoringService.main(dbname, user, password, host, port, pattern)
    # unregister_df = pg_lldService.get_unregister_traffic(dbname, user, password, host, port)
    # st.markdown(get_table_download_link(unregister_df), unsafe_allow_html=True)


# if finish is True:
#     st.success("Готово! Приложение успешно настроено. Приятной работы!")
