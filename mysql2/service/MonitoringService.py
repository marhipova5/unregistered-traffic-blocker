from mysql2.service.LldService import LldService
import time
from mysql2.service.MySQLService import MySQlService
import pickle

lldService = LldService()
mySQLService = MySQlService()

filename = "../../postgresql/service/lof_finalized_model.sav"
loaded_model = pickle.load(open(filename, 'rb'))

class MonitoringService:

    def run(self, dbname, user, password, host, port, pattern):
        active_queries = mySQLService.get_active_query(dbname, user, password, host, port)
        for x in active_queries:
            query = x[7]
            if pattern not in query:
                vector = lldService.get_vector(query, dbname, user, password, host, port)
                result = loaded_model.predict(vector)
                if result == -1:
                    id = x[0]
                    mySQLService.kill_query(id, dbname, user, password, host, port)


    def main(self, dbname, user, password, host, port, pattern):
        while 1:
            self.run(dbname, user, password, host, port, pattern)
            time.sleep(1)
