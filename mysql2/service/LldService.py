import pandas as pd
import itertools
import operator

from postgresql.dto.lldDto import lldDto
from mysql2.service.GldService import GldService
from mysql2.service.MySQLService import MySQlService

gldService = GldService()
db = MySQlService()

class LldService:

    # первичная инициализация таблиц
    def create_lld_table(self, lld_tablename, counter_tablename, dbname, user, password, host, port):
        db.create_table_for_lld(lld_tablename, counter_tablename, dbname, user, password, host, port)

    def get_sql_query_from_df(self, df, lld_tablename, counter_tablename, dbname, user, password, host, port):
        for index, row in df.iterrows():
            self.create_lld_and_safe_into_db(row['message'][10:], lld_tablename, counter_tablename, dbname,
                                             user, password, host, port)

    def get_vector(self, message, dbname, user, password, host, port):
        gld = gldService.get_gld_values_from_db(dbname, user, password, host, port)
        lld = []
        for i in message.split():
            if i.upper() in gld:
                lld.append(i.upper())
        size = len(lld)
        dict = {i: lld.count(i) for i in lld}
        for key, value in dict.items():
            dict[key] = value / size
        # print(dict): {'SELECT': 0.33333333, 'DISTINCT': 0.33333333, 'FROM': 0.33333333}
        df = pd.DataFrame(columns=gld)
        df = df.append(dict, ignore_index=True)
        # Замена Nan на 0.0
        df = df.fillna(0)
        return df

    # заполнить lld таблицу
    def create_lld_and_safe_into_db(self, message, lld_tablename, counter_tablename,
                                    dbname, user, password, host, port):
        gld = gldService.get_gld_values_from_db(dbname, user, password, host, port)
        lld = []
        for i in message.split():
            if i.upper() in gld:
                lld.append(i.upper())
        # print(lld): ['SELECT', 'DISTINCT', 'FROM']
        size = len(lld)
        dict = {i: lld.count(i) for i in lld}
        # print(dict): {'SELECT': 1, 'DISTINCT': 1, 'FROM': 1}
        for key, value in dict.items():
            dict[key] = value / size
        # print(dict): {'SELECT': 0.33333333, 'DISTINCT': 0.33333333, 'FROM': 0.33333333}
        self.safe_into_lld_table(dict, lld_tablename, counter_tablename,
                                 dbname, user, password, host, port)
        return lld

    def safe_into_lld_table(self, dict, lld_tablename, counter_tablename, dbname, user, password, host, port):
        db.safe_into_lld_table(dict, lld_tablename, counter_tablename, dbname, user, password, host, port)

    def get_lld_values_from_db(self, tablename, dbname, user, password, host, port):
        result = [lldDto(r[1], r[2], r[3]) for r in db.select_all(tablename, dbname, user, password, host, port)]
        return result

    def get_matrix(self, tablename, dbname, user, password, host, port):
        values = self.get_lld_values_from_db(tablename, dbname, user, password, host, port)
        gld = gldService.get_gld_values_from_db(dbname, user, password, host, port)
        df = pd.DataFrame(columns=gld)
        # группируем по id_query
        get_attr = operator.attrgetter('id_query')
        new_list = [list(g) for k, g in itertools.groupby(sorted(values, key=get_attr), get_attr)]
        # для каждого запроса формируем dict и записываем в DataFrame
        for x in new_list:
            val = {}
            for y in x:
                val[y.key.upper()] = y.value
            df = df.append(val, ignore_index=True)
        # Замена Nan на 0.0
        df = df.fillna(0)
        return df

    def create_table_for_unregistered_lld(self, dbname, user, password, host, port):
        db.create_table_for_lld('unregistr_lld', 'unregistr_counter', dbname, user, password, host, port)