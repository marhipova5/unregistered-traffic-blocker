import mysql.connector
from mysql.connector import Error

class MySQlService:

    def create_table_for_lld(self, tablename, counter_tablename, dbname, user, password, host, port):
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            sql1 = '''CREATE TABLE IF NOT EXISTS ''' + tablename + '''(id serial PRIMARY KEY, key varchar(100), value float, id_query bigint)'''
            sql2 = '''CREATE TABLE IF NOT EXISTS ''' + counter_tablename + '''(id serial PRIMARY KEY, value bigint)'''
            sql3 = '''INSERT INTO ''' + counter_tablename + '''(value) VALUES (0)'''
            cursor.execute(sql1)
            cursor.execute(sql2)
            cursor.execute(sql3)
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()

    def safe_into_lld_table(self, dict, lld_tablename, counter_tablename,
                            dbname, user, password, host, port):
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            sql = '''INSERT INTO ''' + lld_tablename + '''(key, value, id_query) VALUES ('''
            cursor.execute("SELECT value FROM " + counter_tablename + " WHERE id = 1")
            count = cursor.fetchone()
            count = count[0] + 1
            for key, value in dict.items():
                sql1 = sql + '''\'''' + key + '''\',''' + str(value) + ''',''' + str(count) + ''')'''
                cursor.execute(sql1)
                cursor.execute('''UPDATE ''' + counter_tablename + ''' SET value = ''' +
                             str(count) + ''' WHERE id = 1''')
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()

    def get_active_query(self, dbname, user, password, host, port):
        result = []
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            cursor.execute("show processlist")
            result = cursor.fetchall()
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
        return result

    def kill_query(self, id, dbname, user, password, host, port):
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            sql = '''kill ''' + id
            cursor.execute(sql)
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
