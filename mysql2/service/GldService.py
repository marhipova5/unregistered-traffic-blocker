import xlrd
import mysql.connector
from mysql.connector import Error

class GldService:

    def get_gld_values_from_xlsx_file(self):
        postgresql_file = xlrd.open_workbook("C:/D/Diplom/Project/file/GLD_MySQL.xlsx")
        sheets = postgresql_file.sheet_names()
        required_data = []
        for sheet_name in sheets:
            sh = postgresql_file.sheet_by_name(sheet_name)
            for rownum in range(0, sh.nrows):
                row_valaues = sh.row_values(rownum)
                required_data.append((row_valaues[0]))
        res = []
        [res.append(x) for x in required_data if x not in res]
        print(res)
        return res

    def safe_gld_values_into_db(self, mysql_gld, dbname, user, password, host, port):
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            sql = '''CREATE TABLE IF NOT EXISTS mysql_gld (key_word varchar(100))'''
            cursor.execute(sql)
            for x in mysql_gld:
                cursor.execute("INSERT INTO mysql_gld (key_word) VALUES (%s)", (x,))
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()


    def get_gld_values_from_db(self, dbname, user, password, host, port):
        rows = []
        try:
            conn = mysql.connector.connect(host=host, database=dbname, user=user,
                                           password=password, port=port)
            cursor = conn.cursor()
            cursor.execute("SELECT DISTINCT key_word FROM postgresql_gld")
            tuples_list = cursor.fetchall()
            # tuples_list = [('ABORT',), ('ABS',), ('ABSENT',), ('ABSOLUTE',), ... ]
            rows = [x[0] for x in tuples_list]
            conn.commit()
        except Error as e:
            print(e)
        finally:
            cursor.close()
            conn.close()
        return rows
