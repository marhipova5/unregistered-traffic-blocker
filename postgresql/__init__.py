import pandas as pd
# import xlrd
# import psycopg2
# from psycopg2 import sql
import csv
import matplotlib.pyplot as plt
import seaborn as sns
# import collections
#
from postgresql.service.GldService import GldService
# from postgresql.service.LldService import LldService
#
# # from postgresql.service.LogService import LogService
#
#
gldService = GldService()
# lldService = LldService()
#
#
# # logService = LogService()
#
def get_query_statistics():
    gld = gldService.get_gld_values_from_db('diplom2', 'postgres', '1234', 'localhost', '5432')
    queries = []
    with open("C:/D/Diplom/other/Examples/register_traffic.csv") as tsv_file:
        tsv_reader = csv.reader(tsv_file, delimiter=",")
        for line in tsv_reader:
            lld = ""
            for i in line[13].split():
                if i.upper() in gld:
                    lld = lld + ' ' + i.upper()
            #    удалить пробел в начале строки
            lld = lld.lstrip()
            queries.append(lld)
    return queries


def visualize_query_statistics(l):
    d = {}
    df = pd.DataFrame(columns=['query', 'count'])
    for query in l:
        # if we have not seen team before, create k/v pairing
        # setting value to 0, if team already in dict this does nothing
        d.setdefault(query, 0)
        # increase the count for the team
        d[query] += 1
    for query, count in d.items():
        df = df.append({'query': query,'count': count}, ignore_index=True)
        # print("{} {}".format(query, count))
    # print(df)
    df.to_csv(r'C:/D/Diplom/other/Examples/export_dataframe.csv', index=False, header=False)
    sns.countplot(l)
    plt.show()

    # count_classes = pd.value_counts(l, sort=True)
    # count_classes.plot(kind='bar', rot=0)
    # plt.title("Transaction Class Distribution")
    # plt.xticks(range(2), LABELS)
    # plt.xlabel("Class")
    # plt.ylabel("Frequency");
#
#
if __name__ == "__main__":
    q = get_query_statistics()
    visualize_query_statistics(q)
#     # 1 version
#     # postgresql_gld = gldService.get_gld_values_from_xlsx_file()
#     # gldService.safe_gld_values_into_db(postgresql_gld)
#     # lldService.create_table_for_lld()
#
#     # gld = gldService.get_gld_values_from_db()
#     # with open("C:/Program Files/PostgreSQL/12/data/log/postgresql.csv") as csv_file:
#     #     csv_reader = csv.reader(csv_file, delimiter=',')
#     #     # Loop until we have parsed all the lines.
#     #     for line in csv_reader:
#     #         if "statement:" in line[13]:  # column - message
#     #             lldService.create_lld_and_safe_into_db(line[13][10:], gld)  # [10:] - substring
#
#     # 2 version
#     # postgresql_gld = gldService.get_gld_values_from_xlsx_file()
#     # gldService.safe_gld_values_into_db(postgresql_gld)
#     # lldService.create_table_for_lld('lld5', 'counter5')
#     # lldService.create_table_for_lld('unregistr_lld5', 'unregister_counter5')
#
#     # gld = gldService.get_gld_values_from_db()
#     # print(gld)
#     # with open("C:/D/Diplom/other/mysql_examples/1.txt") as tsv_file:
#     #     tsv_reader = csv.reader(tsv_file, delimiter="\t")
#     #     for line in tsv_reader:
#     #         print(line)
#     #         print(line[1])
#     #         lldService.create_lld_and_safe_into_db2(line[1], gld)
#
#     # matrix = lldService.get_matrix2('lld2')
#     # print(matrix)
#

#     unreqistr = lldService.get_matrix('unregistr_lld')
#     print(unreqistr)
