class lldDto:

    def __init__(self, key, value, id_query):
        self.key = key
        self.value = value
        self.id_query = id_query

    def __repr__(self):
        return "lldDTo key:% s value:% s id_query:% s" % (self.key, self.value, self.id_query)