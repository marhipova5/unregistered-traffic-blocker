from postgresql.service.LldService import LldService
import time
from postgresql.service.PostgreSQLService import PostgreSQLService
import pickle

lldService = LldService()
postgreSQLService = PostgreSQLService()

# pattern = "/*comment*/"
# dbname = 'diplom2'
# user = 'postgres'
# password = '1234'
# host = 'localhost'
# port = '5432'
filename = "C:\D\Diplom\Project\postgresql\service\lof_finalized_model.sav"
loaded_model = pickle.load(open(filename, 'rb'))

class MonitoringService:

    def run(self, dbname, user, password, host, port, pattern):
        postgreSQLService.create_table_for_unregistered_traffic(dbname, user, password, host, port)
        active_queries = postgreSQLService.get_active_query(dbname, user, password, host, port)
        for x in active_queries:
            query = x[18]
            if pattern not in query:
                vector = lldService.get_vector(query, dbname, user, password, host, port)
                result = loaded_model.predict(vector)
                if result == -1:
                    pid = x[2]
                    postgreSQLService.safe_unregistered_traffic(query, dbname, user, password, host, port)
                    postgreSQLService.kill_query(pid, dbname, user, password, host, port)


    def main(self, dbname, user, password, host, port, pattern):
        while 1:
            self.run(dbname, user, password, host, port, pattern)
            time.sleep(1)
