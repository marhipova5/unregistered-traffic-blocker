import psycopg2
import re

class PostgreSQLService:
    count = 0

    # def __init__(self):
    #     self.dbname = dbname
    #     self.user = user
    #     self.password = password
    #     self.host = host
    #     self.port = port

    def __connect__(self, dbname, user, password, host, port):
        self.con = psycopg2.connect(dbname=dbname, user=user, password=password,
                                    host=host, port=port)
        self.cur = self.con.cursor()
        self.con.autocommit = True

    def __disconnect__(self):
        self.cur.close()
        self.con.close()

    def fetch(self, sql, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def execute(self, sql, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        self.cur.execute(sql)
        self.__disconnect__()

    def create_table_for_lld(self, tablename, counter_tablename, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''CREATE TABLE IF NOT EXISTS ''' + tablename + '''(id serial PRIMARY KEY, key varchar(100), value float, id_query bigint)'''
        self.cur.execute(sql)
        self.cur.execute('''CREATE TABLE IF NOT EXISTS ''' + counter_tablename + '''(id serial PRIMARY KEY, value bigint)''')
        self.cur.execute('''INSERT INTO ''' + counter_tablename + '''(value) VALUES (0)''')
        self.__disconnect__()

    def select_all(self, tablename, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''SELECT * FROM ''' + tablename
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def safe_into_lld_table(self, dict, lld_tablename, counter_tablename,
                            dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''INSERT INTO ''' + lld_tablename + '''(key, value, id_query) VALUES ('''
        self.cur.execute("SELECT value FROM " + counter_tablename + " WHERE id = 1")
        count = self.cur.fetchone()
        count = count[0] + 1
        for key, value in dict.items():
            sql1 = sql + '''\'''' + key + '''\',''' + str(value) + ''',''' + str(count) + ''')'''
            self.cur.execute(sql1)
            self.cur.execute('''UPDATE ''' + counter_tablename + ''' SET value = ''' +
                             str(count) + ''' WHERE id = 1''')
        self.con.commit()
        self.__disconnect__()

    def get_active_query(self, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        self.cur.execute("SELECT * FROM pg_stat_activity WHERE state = 'active'")
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

    def kill_query(self, pid, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''SELECT pg_cancel_backend(''' + pid + ''')'''
        self.cur.execute(sql)
        self.con.commit()
        self.__disconnect__()

    def create_table_for_unregistered_traffic(self, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''CREATE TABLE IF NOT EXISTS unregister_query(id bigint PRIMARY KEY, query text)'''
        self.cur.execute(sql)
        self.__disconnect__()

    def safe_unregistered_traffic(self, query, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        query = query.replace('"', '')
        sql = '''INSERT INTO unregister_query(query) VALUES (''' + query + ''')'''
        self.cur.execute(sql)
        self.con.commit()
        self.__disconnect__()

    def get_all_unregistered_traffic(self, dbname, user, password, host, port):
        self.__connect__(dbname, user, password, host, port)
        sql = '''SELECT * FROM unregister_query'''
        self.cur.execute(sql)
        result = self.cur.fetchall()
        self.__disconnect__()
        return result

# SELECT * FROM pg_stat_activity WHERE state = 'active';
# SELECT pg_cancel_backend(<pid of the process>)
